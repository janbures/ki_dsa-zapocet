﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatoveStruktury1
{
    class BubbleSort
    {

        public List<int> Sort(List<int> intList)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            bool end = true;
            // -1 protože poslední prvek v listu už bude seřazen a nebude potřeba ho znova sortovat
            for (int i = 0; i < intList.Count - 1; i++)
            {
                for (int j = 0; j < intList.Count - i - 1; j++)
                {
                    // pokud je nasledujici prohledavany prvek menší než aktuální prohledavany prvek tak prvky prohodím a nastavím end na false
                    // pokud by se flag nenastavil na false - znamenalo by to že v sortu nebyly prohozeny žádné prvky a list je spravně seřazen - poté skončí
                    if (intList[j + 1] < intList[j])
                    {
                        int tmp = intList[j + 1];
                        intList[j + 1] = intList[j];
                        intList[j] = tmp;
                        end = false;
                    }
                }
                if (end)
                {
                    break;
                }
            }
            stopWatch.Stop();
            Console.WriteLine("Bubble sort seřadil list v čase "+ stopWatch.Elapsed);
            Console.WriteLine("-----------------");

            
            return intList;

        }
    }
}
