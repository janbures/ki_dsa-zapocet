﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatoveStruktury1
{
    class InsertionSort
    {
        public List<int> Sort(List<int> intList)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            int n = intList.Count;
            //Cykl pojede od 1 protože první [0] hodnotu na začatku volím už jako "seřazenou"
            for (int i = 1; i < n; ++i)
            {
                //Volím si jakou hodnotu budu porovnávat
                int key = intList[i];
                //porovnávám všechny hodnoty před porovnávanou hodnoutou
                int j = i - 1;

                //Porovnavam do doby dokud je porovnávání stále v rozsahu pole a zaroven je porovnavana hodnota stále větší než key
                while (j >= 0 && intList[j] > key)
                {
                    intList[j + 1] = intList[j];
                    j = j - 1;
                }
                intList[j + 1] = key;
            }
            stopWatch.Stop();
            Console.WriteLine("Insertion sort seřadil list v čase " + stopWatch.Elapsed);
            Console.WriteLine("-----------------");
            return intList;
        }
    }
}
