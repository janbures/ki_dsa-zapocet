﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatoveStruktury1
{

    class QuickSort
    {
        public List<int> Sort(List<int> intList, int low, int high)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            QuickSorting(intList,low,high);
            Console.WriteLine("Insertion sort seřadil list v čase " + stopWatch.Elapsed);
            Console.WriteLine("-----------------");
            return intList;
        }
        static void QuickSorting(List<int> intList, int low, int high)
        {
            if (low < high)
            {

                /* pi is partitioning index, arr[pi] is  
                now at right place */
                int pi = partition(intList, low, high);

                //Rekurzivně sorutuju elementy předtim než znovu rozdělím list
                QuickSorting(intList, low, pi - 1);
                QuickSorting(intList, pi + 1, high);
            }           
            

        }

        static int partition(List<int> intList, int low, int high)
        {
            int pivot = intList[high];

            // Ukladani indexu menšího elementu
            int i = (low - 1);
            for (int j = low; j < high; j++)
            {
                // Pokud je prohledavaný element menší ne pivot
                if (intList[j] < pivot)
                {
                    i++;

                   //Prohazuje intList[i] s intList[j] - SWAP Funkce
                    int temp = intList[i];
                    intList[i] = intList[j];
                    intList[j] = temp;
                }
            }

            // prohození intList[i] and intList[high]
            int temp1 = intList[i + 1];
            intList[i + 1] = intList[high];
            intList[high] = temp1;

            return i + 1;
        }
       
    }
}
