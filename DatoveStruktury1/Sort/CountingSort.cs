﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatoveStruktury1
{
    class CountingSort
    {
        public List<int> Sort(List<int> intList)
        {
            int n = intList.Count();

            // Definuju output pole do kterého budu ukladat seřazené pole
            int[] output = new int[n];

            // Vytvořím si count pole (pole četností) na uklidání počtu jednotlivých hodnot a všechy hodnoty nastavím na nulu 
            int[] count = new int[256];

            for (int i = 0; i < 256; ++i)
            {
                count[i] = 0;
            }
                

            // do pole si uložím četnost každé hodnoty
            for (int i = 0; i < n; ++i)
            {
                ++count[intList[i]];
            }
                

            // Change count[i] so that count[i]  
            // now contains actual position of  
            // this character in output array 

            //změnim pole četností (count[i]) aby obsahoval aktualní pozici této hodnoty v output poli 
            for (int i = 1; i <= 255; ++i)
            {
                count[i] += count[i - 1];
            }

            // Build the output character array 
            // To make it stable we are operating in reverse order. 

            //Nyní seskládám output pole
            for (int i = n - 1; i >= 0; i--)
            {
                //na index s četností prvku ukládám prvek z listu
                output[count[intList[i]] - 1] = intList[i];

                //odečítam z listu četností jedničku ( v listu muže být klidně nastavena hodnota četnosti i na velikost pole 
                --count[intList[i]];
            }


            //Poté jen zkopiruju output pole do listu a list vratim
            for (int i = 0; i < n; ++i)
            {
                intList[i] = output[i];
            }
               

            return intList;
        }
    }
}
