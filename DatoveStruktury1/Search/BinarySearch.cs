﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatoveStruktury1
{
    class BinarySearch
    {
        private int _findNumber;
        private List<int> _intList = new List<int> { };

        public void Menu(List<int> _intList)
        {
            this._intList = _intList;
            Console.WriteLine("Jaké číslo byste chtěli vyhledat ?");
            this._findNumber = Int32.Parse(Console.ReadLine());
            Search();
            Console.WriteLine("------------------");
        }

        public void Search()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            bool found = false;
            int min = 0;
            int max = this._intList.Count - 1;

            while (min <= max && !found)
            {
                int mid = (min + max) / 2;
                if (this._findNumber == this._intList[mid])
                {
                    BruteForce bruteForce = new BruteForce();
                    bruteForce.Search(this._intList, this._findNumber);
                    found = true;

                }
                else if (this._findNumber < this._intList[mid])
                {
                    max = mid - 1;
                }
                else
                {
                    min = mid + 1;
                }
            }
            if (!found)
            {
                Console.WriteLine("Prvek nenalezen");
            }
            else
            {
                Console.WriteLine("Prvek byl nalezen");
            }

            Console.WriteLine("List pomoci Binárního vyhledávání byl probledán v čase " + stopWatch.Elapsed);
            Console.WriteLine("-----------------");
        }
    }
}
