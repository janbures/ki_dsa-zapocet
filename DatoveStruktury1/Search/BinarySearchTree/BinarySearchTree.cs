﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatoveStruktury1.Search.BinarySearchTree
{

    class BinarySearchTree
    {

        //Root uzel který je počátkem celého stromu
        public Node Root = null;

        public BinarySearchTree(List<int> values,int findValue)
        {

            //Vytvoření stromu
            foreach(int value in values)
            {
                this.Insert(value);
            }

            //Hledání hodnoty
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            Node node = this.Find(findValue, this.Root);
            if (node != null)
            {
                stopWatch.Stop();
                Console.WriteLine("Hodnota nalezena v čase "+ stopWatch.Elapsed);
            }
            else{
                stopWatch.Stop();
                Console.WriteLine("Hodnota nenalezena v čase " + stopWatch.Elapsed);
            }
        }

        //Funkce na vložení uzlu do stromu
        public bool Insert(int value)
        {
           
            Node before = null;
            Node after = this.Root;

            //Pokud se nejedná o první vkládání a základ stromu už je založen tak jako předchozí Uzel (používám jako pomocnou proměnnou) 
            //dám nynější uzel a zkontroluju zda hodnota kterou chci vkládat už není obsažená ve stromě aby nevznikali duplíkáty - jak na pravé straně tak na levé straně
            while (after != null)
            {
                before = after;
                if (value < after.value)
                    after = after.left;
                else if (value > after.value)
                    after = after.right;
                else
                {
                    //Pokud hodnota už je obsažena tak uzel nepřidávám
                    return false;
                }
            }

            //Pokud je hodnota ve stromě nová vytvořím nový uzel a uložím do něj hodnotu
            Node newNode = new Node();
            newNode.value = value;

            //Pokud je strom prázdný a ješte nebyl naplněn tak ho "naplním" prvním uzlem, jedná se většinou o první vložení
            if (this.Root == null)
            {
                this.Root = newNode;
            }
           //Pokud strom není prázdný rozhoduju se z jaké strany "ho napojím" pokud je hodnota menší tak doleva pokud větší tak doprava
            else
            {
                if (value < before.value)
                {
                    before.left = newNode;
                }
                else
                {
                    before.right = newNode;
                }
            }

            return true;
        }


        
        private Node Find(int value, Node node)
        {
            //pokud by byl null tak strom bud nebyl naplněn, nebo dojel už dokonce a nic nenašel
            if (node != null)
            {
                //hodnota naleena
                if (value == node.value)
                {
                    return node;
                }
                //hledana hodnota je menší než hodnota uzle vydám se nalevo
                if (value < node.value)
                {
                    return Find(value, node.left);
                }
                //hledana hodnota je větší než hodnota uzle vydám se napravo
                else
                {
                    return Find(value, node.right);
                }
                    
            }

            return null;
        }
    }
}
