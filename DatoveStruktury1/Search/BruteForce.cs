﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatoveStruktury1
{
    class BruteForce
    {
        public void Search(List<int> intList, int foundNumber)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            int foundXTimes = 0;
            for (int i = 0; i < intList.Count; i++)
            {
                if (intList[i] == foundNumber)
                {
                    Console.WriteLine("Prvek " + foundNumber + " nalezen na pozici " + i);
                    foundXTimes++;
                }
            }
            Console.WriteLine("Prvek byl nalezen "+foundXTimes+"x.");
            Console.WriteLine("List pomocí Brute force byl probledán v čase " + stopWatch.Elapsed);
            Console.WriteLine("-----------------");
        }

    }
}
