﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatoveStruktury1
{
    class Lifo
    {
        private int _vrcholZasobniku = 0;
        private int[] _arr;

        public void lifoMenu(int arrSize)
        {
            this._arr = new int[arrSize];
            bool end = true;
            while (end)
            {
                Console.WriteLine("-----------------");
                Console.WriteLine("Zvolte jakou akci chcete vykonat \n 1. Odebrat \n 2. Přidat \n 3. Konec");
                int option = Int32.Parse(Console.ReadLine());
                Console.WriteLine("-----------------");
                if (option == 1)
                {
                    RemoveFromLifo();
                }
                else if (option == 2)
                {
                    Console.WriteLine("Zadejte číslo které chcete přidat");
                    AddToLifo(Int32.Parse(Console.ReadLine()));
                }
                else
                {
                    end = false;
                }

            }
        }


        public void AddToLifo(int number)
        {
            try
            {
                this._arr[_vrcholZasobniku] = number;
                this._vrcholZasobniku++;
            }
            catch (IndexOutOfRangeException ire)
            {
                Console.WriteLine("Zásobník přetekl");
            }

        }

        public void RemoveFromLifo()
        {
            if (this._vrcholZasobniku == 0)
            {
                Console.WriteLine("Zásobník je prázdný");
            }
            else
            {
                this._vrcholZasobniku--;
                Console.WriteLine("Prvek odebrán (" + this._arr[this._vrcholZasobniku] + ")");
                
            }
        }



    }
}
