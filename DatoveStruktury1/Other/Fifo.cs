﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatoveStruktury1
{
    class Fifo
    {
        private int[] _arr;

        //čtecí hlava
        int start   = 0;
        //zapisovací hlava
        int end     = 0;

        public void fifoMenu(int arrSize)
        {
            this._arr = new int[arrSize];
            bool end = true;
            while (end)
            {
                Console.WriteLine("-----------------");
                Console.WriteLine("Zvolte jakou akci chcete vykonat \n 1. Odebrat \n 2. Přidat \n 3. Konec");
                int option = Int32.Parse(Console.ReadLine());
                Console.WriteLine("-----------------");
                if (option == 1)
                {
                    RemoveFromFifo();
                }
                else if (option == 2)
                {
                    Console.WriteLine("Zadejte číslo které chcete přidat");
                    AddToFifo(Int32.Parse(Console.ReadLine()));
                }
                else
                {
                    end = false;
                }

            }
        }
        public void AddToFifo(int num)
        {
            int tmp = 0;

            //Když by byla velikost pole stejna nebo větší než zapisovací hlava, tak budu přepisovat hodnotu ktere je "v kruhu na řadě"
            if (this.end + 1 >= this._arr.Count())
            {
                tmp = this.end + 1 - this._arr.Count();
            }
            // Pokud je menší tak zapisovací hlavu pouze posunu o jednu
            else
            {
                tmp = this.end + 1;
            }
            // Pokud nastane že zapisovaci hlava bude na stejne jako čtecí hlava tak nebudu moci prvek přepsat
            if (tmp == start)
            {
                Console.WriteLine("Prvek nelze přidat");
            }
            else
            {
                //Přidavani do fronty
                this._arr[end] = num;
                end++;

                //Pokud zapisovaci hlava přesahne limit tak jí nastavim opět na začátek
                if (this.end == this._arr.Count())
                {
                    this.end = 0;
                }
            }
        }
        public void RemoveFromFifo()
        {
            //Pokud není zapisovací hlava na stejnem místě jako čtecí hlava tak jí 
            if (this.start != this.end)
            {
                Console.WriteLine("Prvek odebrán (" + this._arr[this.start] + ")");

                this.start++;
                //Pokud čtecí hava přesáhne velikost pole tak jí nastavim na začátek fronty
                if (this.start >= this._arr.Count())
                {
                    this.start = 0;
                }      
            }
            else
            {
                Console.WriteLine("Fronta je prázdná a nelze z ní nic odebrat.");
            }
        }
    }
}
