﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatoveStruktury1
{
    class LFMenu
    {
        public void lifoFifOMenu()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("Zvolte o jaký typ se bude jednat \n 1 - Zásobník \n 2 - Fronta ");
            int option = Int32.Parse(Console.ReadLine());
            Console.WriteLine("-----------------");
            if(option == 1)
            {
                Console.WriteLine("Zvolil/a jste zásobník.");
                Console.WriteLine("-----------------");
                Console.WriteLine("Určete jak bude velký zásobník");
            }
            else
            {
                Console.WriteLine("Zvolil/a jste frontu.");
                Console.WriteLine("-----------------");
                Console.WriteLine("Určete jak bude velká fronta");
            }

            int arrSize = Int32.Parse(Console.ReadLine());
            if (option == 1)
            {
                Lifo lifo = new Lifo();
                lifo.lifoMenu(arrSize);
            }
            else if (option == 2)
            {
                Fifo fifo = new Fifo();
                fifo.fifoMenu(arrSize);
            }
            Console.WriteLine("-----------------");
        }
    }
}
