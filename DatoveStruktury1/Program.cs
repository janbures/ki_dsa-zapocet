﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatoveStruktury1.Search.BinarySearchTree;

namespace DatoveStruktury1
{
    class Program
    {
        public static void Main(string[] args)
        {
            BubbleSort bubbleSort       = new BubbleSort();
            InsertionSort insertionSort = new InsertionSort();
            CountingSort countingSort   = new CountingSort();
            BinarySearch binarySearch   = new BinarySearch();
            QuickSort quickSort         = new QuickSort();
            LFMenu lfmenu               = new LFMenu();
            
            BruteForce bruteForce       = new BruteForce();

            //lfmenu.lifoFifOMenu();

            //Do souboru cisla.txt se uloží 16 čísel od 0 do 100 dělená čárkou
            generateRandNumber(0, 10, 30);

            //převede čísla ze souboru cisla.txt do listu a poté pomocí bubble sortu prosortuje a vrátí v bubble-sort.txt
            //SaveIntoFile(bubbleSort.Sort(fileToList("cisla")), "bubble-sort");

            //převede čísla ze souboru cisla.txt do listu a poté pomocí insertion sortu prosortuje a vrátí v insertion-sort.txt
            //SaveIntoFile(insertionSort.Sort(fileToList("cisla")), "insertion-sort");

            //převede čísla ze souboru cisla.txt do listu a poté pomocí counting-sortu prosortuje a vrátí v counting-sort.txt
            SaveIntoFile(countingSort.Sort(fileToList("cisla")), "counting-sort");

            //převede čísla ze souboru cisla.txt do listu a poté pomocí quick-sortu prosortuje a vrátí v counting-sort.txt

            SaveIntoFile(quickSort.Sort(fileToList("cisla"), 0, fileToList("cisla").Count()-1), "quick-sort");
            //bruteForce.Search(fileToList("insertion-sort"),8);
            //binarySearch.Menu(fileToList("insertion-sort")); 
            Console.WriteLine("Zadejte jaké číslo chcete najít pomocí Binarního stromu");
            BinarySearchTree bts = new BinarySearchTree(fileToList("cisla"),Int32.Parse(Console.ReadLine()));


            Console.ReadKey();
        }
        public static void generateRandNumber(int from, int to, int howMany)
        {
            Random rnd = new Random();
            string numbers = "";
            for(int i = 0; i<howMany; i++)
            {
                numbers = numbers+rnd.Next(from,to)+",";
            }
            System.IO.File.WriteAllText(@"cisla.txt", numbers);
        }
    
        public static List<int> fileToList(string fileName)
        {
            List<int> intList = new List<int>();
            string text = System.IO.File.ReadAllText(@""+fileName+".txt");
            string[] textArray = text.Split(',');
            for (int i = 0;i< textArray.Length;i++)
            {
                try
                {
                    intList.Add(Int32.Parse(textArray[i]));
                }
                catch(Exception e)
                {

                }
            }
            return intList;
        }

        public static void SaveIntoFile(List<int> intList, string fileName)
        {
            string numbers = "";
            for (int x = 0; x < intList.Count; x++)
            {
                numbers = numbers + intList[x] + ",";
            }
            System.IO.File.WriteAllText(@""+fileName+".txt", numbers);
        }
    }
}

/*
 Zápočet: 
    1. zásobník
    2. cyklická fronta
    3. generátor náhodných čísel
    4. Bubble sort / Insert sort / Select sort - vyzkoušet do 50 000
5. Quick sort / Merge sort - vyzkoušet do 1 000 000
6. Counting sort / Radix sort / Bucket sort - vyzkoušet do 20 000 000 (každý jinej soubor)
7. Binární vyhledávání + Brute force
8. BST (Binární vyhledávací strom) / jiné vyhledávací stromové struktury / CHT (hashovací tabulky)
**/